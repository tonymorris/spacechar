0.0.0.5

* Remove unit data types.

0.0.0.4

* Fix export list

0.0.0.3

* Add prism instances for `Char`.

0.0.0.2

* Export list and haddock documentation.

0.0.0.1

* The initial version of spacechar.
